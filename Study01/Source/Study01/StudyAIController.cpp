// Fill out your copyright notice in the Description page of Project Settings.

#include "StudyAIController.h"
#include "NavigationSystem.h"
#include "BluePrint/AIBlueprintHelperLibrary.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"

const FName AStudyAIController::homePosKey(TEXT("HomePos"));
const FName AStudyAIController::patrolPosKey(TEXT("PatrolPos"));

AStudyAIController::AStudyAIController()
{
	repeatInterval = 3.0f;

	static ConstructorHelpers::FObjectFinder<UBlackboardData> BBObject(TEXT("/Game/AI/StudyBB"));
	if (BBObject.Succeeded() == true) 
	{
		bbAsset = BBObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<UBehaviorTree> BTObject(TEXT("/Game/AI/StudyBT"));
	if (BTObject.Succeeded() == true)
	{
		btAsset = BTObject.Object;
	}
}

void AStudyAIController::Possess(APawn* pawn)
{
	Super::Possess(pawn);

	//UWorld* world = GetWorld();
	//FTimerManager& tm = world->GetTimerManager();
	//tm.SetTimer(repeatTimeHandle, this, &AStudyAIController::OnRepeatTimer, repeatInterval, true);

	if (UseBlackboard(bbAsset, Blackboard) == false)
	{
		UE_LOG(tann, Log, TEXT("blackboard setup fail."));
		return;
	}

	Blackboard->SetValueAsVector(homePosKey, pawn->GetActorLocation());

	if (RunBehaviorTree(btAsset) == false) 
	{
		UE_LOG(tann, Log, TEXT("behavior tree setup fail."));
		return;
	}
	UE_LOG(tann, Log, TEXT("ai posses complete."));
}


void AStudyAIController::UnPossess()
{
	Super::UnPossess();
	UWorld* world = GetWorld();
	world->GetTimerManager().ClearTimer(repeatTimeHandle);
}

void AStudyAIController::OnRepeatTimer()
{
	APawn* currentPawn = GetPawn();
	// UNavigationSystem::GetNavigationtNavigationSystem(GetWorld());
	UNavigationSystemV1* navSystem = UNavigationSystemV1::GetNavigationSystem(GetWorld());
	if (nullptr == navSystem) return;
	FNavLocation nextLocation;
	bool result = navSystem->GetRandomPointInNavigableRadius(FVector::ZeroVector, 500.0f, nextLocation);
	if (result == true) 
	{
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, nextLocation);
	}
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Study01.h"
#include "AIController.h"
#include "StudyAIController.generated.h"

/**
 * 
 */
UCLASS()
class STUDY01_API AStudyAIController : public AAIController
{
	GENERATED_BODY()
public:

	AStudyAIController();
	virtual void Possess(APawn* pawn) override;
	virtual void UnPossess() override;

	static const FName homePosKey;
	static const FName patrolPosKey;

private:
	void OnRepeatTimer();

	FTimerHandle repeatTimeHandle;
	float repeatInterval;

	UPROPERTY()
	class UBehaviorTree* btAsset;
	
	UPROPERTY()
	class UBlackboardData* bbAsset;
};

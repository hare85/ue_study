// Fill out your copyright notice in the Description page of Project Settings.

#include "StudyAnimInstance.h"
#include "StudyCharacter.h"

UStudyAnimInstance::UStudyAnimInstance() 
{
	UE_LOG(tann, Log, TEXT("UStudyAnimInstance contructor start"));

	characterState = 0;

	static ConstructorHelpers::FObjectFinder<UAnimMontage> ATTACK_MONTAGE(TEXT("/Game/AdvancedLocomotionV3/Characters/Mannequin/Animations/ALS_Mannequin_Skeleton_Montage"));
	// static ConstructorHelpers::FObjectFinder<UAnimMontage> ATTACK_MONTAGE(TEXT("AnimMontage'/Game/AdvancedLocomotionV3/Characters/Mannequin/Animations/ALS_Mannequin_Skeleton_Montage_withsection.ALS_Mannequin_Skeleton_Montage_withsection'"));
	if (ATTACK_MONTAGE.Succeeded() == true)
	{
		attackMontage = ATTACK_MONTAGE.Object;
	}

	UE_LOG(tann, Log, TEXT("UStudyAnimInstance contructor end"));
}

void UStudyAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	APawn* pawn = TryGetPawnOwner();
	if (::IsValid(pawn) == true)
	{
		AStudyCharacter* character = Cast<AStudyCharacter>(pawn);
		
		if(character == nullptr) return;
		
		currentPawnSpd = character->GetVelocity().Size();
		UCharacterMovementComponent* cmc = character->GetCharacterMovement();
		isJump = cmc->IsFalling();
		// UE_LOG(tann, Log, TEXT("do something"));
	}
}

void UStudyAnimInstance::Jump()
{
	APawn* pawn = TryGetPawnOwner();
	if (::IsValid(pawn) == true)
	{
		AStudyCharacter* character = Cast<AStudyCharacter>(pawn);
		character->Jump();
	}
}

void UStudyAnimInstance::PlayAttackMontage() 
{
	// if (Montage_IsPlaying(attackMontage) == false) {
		Montage_Play(attackMontage, 1.0f);
	// }
}

void UStudyAnimInstance::AnimNotify_FirstAttackPoint()
{
	Montage_JumpToSection(TEXT("attack1"), attackMontage);
	UE_LOG(tann, Log, TEXT("first notify"));
}

void UStudyAnimInstance::AnimNotify_SecondAttackPoint()
{
	Montage_JumpToSection(TEXT("attack2"), attackMontage);
	UE_LOG(tann, Log, TEXT("second notify"));
}

void UStudyAnimInstance::AnimNotify_ThirdAttackPoint()
{
	UE_LOG(tann, Log, TEXT("third notify"));
}
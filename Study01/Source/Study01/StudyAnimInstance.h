#pragma once

#include "Study01.h"
#include "Animation/AnimInstance.h"
#include "StudyAnimInstance.generated.h"

UCLASS(Blueprintable)
class STUDY01_API UStudyAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	UStudyAnimInstance();

	UFUNCTION(BlueprintCallable, Category = Pawn, Meta = (AllowPrivateAccess = true))
	void Jump();
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float currentPawnSpd;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	int characterState;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool isJump;


	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = true))
	UAnimMontage* attackMontage;

	void PlayAttackMontage();

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

private:
	UFUNCTION()
	void AnimNotify_FirstAttackPoint();
	UFUNCTION()
	void AnimNotify_SecondAttackPoint();
	UFUNCTION()
	void AnimNotify_ThirdAttackPoint();
};

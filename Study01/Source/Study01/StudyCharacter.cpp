// Fill out your copyright notice in the Description page of Project Settings.

#include "StudyCharacter.h"
#include "StudyAnimInstance.h"
#include "DrawDebugHelpers.h"

// Sets default values
AStudyCharacter::AStudyCharacter()
{
	UE_LOG(tann, Log, TEXT("AStudyCharacter contructor start"));

 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	springArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("springArm"));
	camera = CreateDefaultSubobject<UCameraComponent>(TEXT("camera"));

	springArm->SetupAttachment(GetCapsuleComponent());
	camera->SetupAttachment(springArm);

	springArm->TargetArmLength = 350.0f;
	springArm->SetRelativeRotation(FRotator(-15.0f, 0, 0));

	GetMesh()->SetRelativeLocationAndRotation(FVector(0, 0, -80), FRotator(0, -90, 0));

	ConstructorHelpers::FObjectFinder<USkeletalMesh> meshObj(TEXT("/Game/AdvancedLocomotionV3/Characters/Mannequin/Mannequin"));
	
	if (meshObj.Succeeded() == true)
	{
		GetMesh()->SetSkeletalMesh(meshObj.Object);
	}

	// by animation blueprint
	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);

	static ConstructorHelpers::FClassFinder<UAnimInstance> animBP(TEXT("/Game/AnimBluePrint/StudyAnimBP"));
	// static ConstructorHelpers::FClassFinder<UStudyAnimInstance> animBP(TEXT("AnimBlueprint'/Game/AnimBluePrint/StateMachineEx.StateMachineEx_C'"));

	if (animBP.Succeeded() == true)
	{
		UE_LOG(tann, Log, TEXT("animBP success"));
		GetMesh()->SetAnimInstanceClass(animBP.Class);
	}
	else
	{
		UE_LOG(tann, Log, TEXT("animBP failure"));
	}

	GetCapsuleComponent()->SetCollisionProfileName(TEXT("StudyPreset"));
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &AStudyCharacter::OnOverlapBegin);
	GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &AStudyCharacter::OnOverlapEnd);
	GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &AStudyCharacter::OnHit);
	
	auto cm = GetCharacterMovement();
	cm->MaxWalkSpeed = 100.0f;
	cm->JumpZVelocity = 1000.0f;

	isAttacking = false;

	UE_LOG(tann, Log, TEXT("AStudyCharacter constructor end"));
}

void AStudyCharacter::PostInitializeComponents() 
{
	Super::PostInitializeComponents();

	auto animInstance = Cast<UStudyAnimInstance>(GetMesh()->GetAnimInstance());
	if (nullptr == animInstance) 
	{
		UE_LOG(tann, Error, TEXT("AStudyCharacter GetAnimInstance fail."));
		return;
	}

	animInstance->OnMontageEnded.AddDynamic(this, &AStudyCharacter::OnAttackMontageEnded);
}

// Called when the game starts or when spawned
void AStudyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}
// Called every frame
void AStudyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//auto cm = GetCharacterMovement();
	//FVector vel = GetVelocity();
}

// Called to bind functionality to input
void AStudyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// PlayerInputComponent->BindAction(TEXT("jump"), IE_Released, this, &AStudyCharacter::AttackCheck);
	PlayerInputComponent->BindAction(TEXT("jump"), IE_Released, this, &AStudyCharacter::AnimChange);
	// PlayerInputComponent->BindAction(TEXT("jump"), IE_Released, this, &AStudyCharacter::ReleaseSpace);

	PlayerInputComponent->BindAction(TEXT("attack"), IE_Released, this, &AStudyCharacter::AttackAction);

	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AStudyCharacter::LeftRight);
	PlayerInputComponent->BindAxis(TEXT("MoveFoward"), this, &AStudyCharacter::UpDown);
}

void AStudyCharacter::UpDown(float scale)
{
	// UE_LOG(tann, Log, TEXT("up down : %f"), scale);
	AddMovementInput(GetActorForwardVector(), scale);
}

void AStudyCharacter::LeftRight(float scale)
{
	// UE_LOG(tann, Log, TEXT("left right : %f"), scale);
	AddMovementInput(GetActorRightVector(), scale);
}

void AStudyCharacter::AnimChange() 
{
	UAnimInstance* animInst = GetMesh()->GetAnimInstance();

	auto studyAnim = Cast<UStudyAnimInstance>(animInst);
	// UStudyAnimInstance* studyAnim = (UStudyAnimInstance *)animInst;
	// studyAnim�� ���� nullüũ �Ҽ� ����.
	// UStudyAnimInstance* studyAnim = dynamic_cast<UStudyAnimInstance *>(animInst);
	// studyAnim�� ���� nullüũ �Ҽ� ����.
	studyAnim->characterState++;
	if (studyAnim->characterState >= 3) studyAnim->characterState = 0;
}

void AStudyCharacter::ReleaseSpace() 
{
	Super::Jump();
}

void AStudyCharacter::AttackCheck()
{
	UE_LOG(tann, Log, TEXT("attack check!!"));

	FHitResult hitResult;
	FCollisionQueryParams params(NAME_None, false, this);
	UWorld* world = GetWorld();

	FVector destPosition = GetActorLocation() + GetActorForwardVector() * 200.0f;

	bool result = world->SweepSingleByChannel(
		hitResult, 
		GetActorLocation(), 
		destPosition, 
		FQuat::Identity, 
		ECollisionChannel::ECC_GameTraceChannel2,
		// FCollisionShape::LineShape,
		FCollisionShape::MakeSphere(50.0f), 
		params
	);

#if ENABLE_DRAW_DEBUG
	FVector traceVec = GetActorForwardVector() * 200.0f;
	FVector center = GetActorLocation() + traceVec * 0.5f;
	float halfHeight = 200.0f * 0.5f + 50.0f;
	FQuat capsuleRot = FRotationMatrix::MakeFromZ(traceVec).ToQuat();
	FColor drawColor = result ? FColor::Green : FColor::Red;
	float DebugLifeTime = 5.0f;

	DrawDebugCapsule(GetWorld(), center, halfHeight, 50.0f, capsuleRot, drawColor, false, DebugLifeTime);
#endif

	if (result == true) 
	{
		if (hitResult.Actor.IsValid() == true) 
		{
			UE_LOG(tann, Log, TEXT("hit actor name : %s"), *hitResult.Actor->GetName());
			FDamageEvent damageEvent;
			hitResult.Actor->TakeDamage(50.0f, damageEvent, GetController(), this);
		}
	}
}

void AStudyCharacter::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult &SweepResult)
{
	UE_LOG(tann, Log, TEXT("overlap begin"));
}

void AStudyCharacter::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
	UE_LOG(tann, Log, TEXT("overlap end"));
}

void AStudyCharacter::OnHit(UPrimitiveComponent* HitComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	// UE_LOG(tann, Log, TEXT("on hit!!"));
}

void AStudyCharacter::AttackAction()
{
	// if (isAttacking == true) return;
	
	isAttacking = true;

	UE_LOG(tann, Log, TEXT("attack action"));
	UAnimInstance* animInst = GetMesh()->GetAnimInstance();
	auto studyAnim = Cast<UStudyAnimInstance>(animInst);
	if (nullptr == studyAnim) {
		return;
	}
	studyAnim->PlayAttackMontage();
}

void AStudyCharacter::OnAttackMontageEnded(UAnimMontage* montage, bool interrupted)
{
	isAttacking = false;
}
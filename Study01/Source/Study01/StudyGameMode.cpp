// Fill out your copyright notice in the Description page of Project Settings.

#include "StudyGameMode.h"
#include "StudyCharacter.h"
#include "StudyPlayerController.h"

AStudyGameMode::AStudyGameMode()
{
	UE_LOG(tann, Log, TEXT("AStudyGameMode contructor start"));
	DefaultPawnClass = AStudyCharacter::StaticClass();
	PlayerControllerClass = AStudyPlayerController::StaticClass();
	UE_LOG(tann, Log, TEXT("AStudyGameMode contructor end"));
}

void AStudyGameMode::PostLogin(APlayerController* NewPlayer)
{
	UE_LOG(tann, Log, TEXT("post login begin"));

	Super::PostLogin(NewPlayer);
	
	UE_LOG(tann, Log, TEXT("post login end"));
}

// Fill out your copyright notice in the Description page of Project Settings.

#include "StudyPlayerController.h"

AStudyPlayerController::AStudyPlayerController() 
{
	UE_LOG(tann, Log, TEXT("AStudyPlayerController contructor start"));
	UE_LOG(tann, Log, TEXT("AStudyPlayerController contructor end"));
}

void AStudyPlayerController::PostInitializeComponents() 
{
	Super::PostInitializeComponents();
	UE_LOG(tann, Log, TEXT("PlayerController initialized"));
}

void AStudyPlayerController::Possess(APawn* aPawn)
{
	UE_LOG(tann, Log, TEXT("PlayerController Possess"));
	Super::Possess(aPawn);
}

void AStudyPlayerController::LeftRight(float scale) 
{
	// do nothing!!
}

void AStudyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	// this makes pawn does not working anymore for 'MoveRight'
	// InputComponent->BindAxis(TEXT("MoveRight"), this, &AStudyPlayerController::LeftRight);
}
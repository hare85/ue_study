// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Study01.h"
#include "GameFramework/PlayerController.h"
#include "StudyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class STUDY01_API AStudyPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AStudyPlayerController();

	virtual void PostInitializeComponents() override;
	virtual void Possess(APawn* aPawn) override;
protected:
	virtual void SetupInputComponent() override;

private:
	void LeftRight(float scale);
};

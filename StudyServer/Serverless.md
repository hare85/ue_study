# Serverless?

## 의미

‘서버리스 컴퓨팅’ 혹은 ‘서버리스 아키텍처’ 의 통칭
'서버가 없다' 라는 의미가 아닙니다.

### 서버리스 컴퓨팅 (Baas: Backend as a Service)

하드웨어 자원의 배분 및 할당을 클라우드에서 동적으로(dynamically) 관리해 주는 것

- Google Firebase
- Aws EC2
- Heroku

### 서버리스 아키택처 (Faas: Function as s Service)

데이터의 기록 및 관리, 사용자 인증, 서비스 구동 등 실제 비즈니스 로직은 서버에서 구현되는 많은 기능들을 클라우드에서 기본 서비스로 제공되는 서비스를 이용하여 서비스 개발자는 백엔드(backend)의 기능적인 측면에 집중할 수 있도록 설계, 운영 하는 것

- Aws Lambda
- Azure Functions
- Google Cloud Functions

# 게임 서버의 Serverless?

## 게임 서버의 기본 구성 요소

- 유저
    + 유저 인증
    + 사용자 허가
    + 유저 행동 분석
- 데이터
    + 유저 데이터 의 서버 저장
    + 공유 데이터 저장
    + 실시간성 데이터 처리
    + real-time 의 경우 패킷 스트리밍
- 미디어
    + 패치
    + 동영상
- 로직
    + 서버
- 메시징
    + 푸쉬

## EX - AWS

AWS를 사용 한다면 위의 게임 서버 구성 요소를 어떻게 serverless하게 만들 수 있는가?

- 유저
    - 유저 인증
      > Amazone Cognito
    - 사용자 허가
      > IAM
    - 유저 행동 분석
      > Amazone Cognito - Mobile Analytics
- 데이터
    - 유저 데이터 의 서버 저장
      >  Amazone Cognito
    - 공유 데이터 저장
      > RDS
    - 실시간성 데이터 처리
      > Kinesis
    - real-time 의 경우 패킷 스트리밍
      > GameLift
- 미디어
    - 패치
      > S3, CloudFront
    - 동영상
      > CloudFront
- 로직
    - 서버
      > Lambda
- 메시징
    - 푸쉬
      > SNS

# 멀티 플레이어 게임

## 싱글과 멀티 플레이어 게임의 이벤트 처리

### 싱글게임

![SingleGame](./img/igc-2017-6-638.jpg)

### 멀티 플레이어 게임

![MultiplayerGame](./img/igc-2017-7-638.jpg)

## 멀티 플레이어 게임 타입

![gametype](./img/igc-2017-8-638.jpg)

## AWS GameLift

Amazon GameLift 는 완전 관리형 서비스로서 클라우드에서 세션 기반 멀티플레이 게임 서버를 배포, 운영, 조정하는 데 사용

### Amazon GameLift의 장점

- 플레이어에게 짧은 지연 시간을 제공하여 빠른 액션 게임 플레이를 지원합니다.
- 백엔드 경험이 거의 또는 전혀 없어도 세션 기반의 멀티플레이어 게임을 빠르게 배포할 수 있습니다.
- 지능형 대기열, 게임 세션 배치 및 채우기 기능으로 매치메이킹 서비스를 개선합니다.
- 게임 서버를 배포하고 운영하는 데 필요한 엔지니어링 및 운영 작업이 적습니다.

----
## 참조

- [아마존의 관리형 게임 플랫폼 활용하기: GameLift (Deep Dive) :: 구승모 솔루션즈 아키텍트 :: Gaming on AWS 2016](https://www.slideshare.net/awskorea/amazon-game-lift-deep-dive-seungmo-koo)
- [[IGC 2017] 아마존 구승모 - 게임 엔진으로 서버 제작 및 운영까지](https://www.slideshare.net/ssuser052dd11/igc-2017)

- [GameLift GameSessions Demo](https://ue4plugin.wordpress.com/2017/06/02/integrate-amazon-gamelift/)

- [AWS-Samples - aws-gamelift-sample](https://github.com/aws-samples/aws-gamelift-sample)
- [AWS-Samples - amazon-gamelift-unity](https://github.com/aws-samples/amazon-gamelift-unity)
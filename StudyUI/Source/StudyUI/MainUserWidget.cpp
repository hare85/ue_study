// Fill out your copyright notice in the Description page of Project Settings.

#include "MainUserWidget.h"

void UMainUserWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	UE_LOG(LogTemp, Log, TEXT("== UMainUserWidget NativeOnInitialized =="));
	
	if (optionWidget != nullptr)
	{
		optionPopup = CreateWidget<UUserWidget>(this, optionWidget);
	}

	UButton* optionBtn = Cast<UButton>(GetWidgetFromName(TEXT("OptionBtn")));
	if (optionBtn != nullptr)
	{
		if (optionBtn->OnClicked.IsBound() == false)
			optionBtn->OnClicked.AddDynamic(this, &UMainUserWidget::OptionBtnClicked);
	}

	UButton* changeBtn = Cast<UButton>(GetWidgetFromName(TEXT("ChangeImageBtn")));
	if (changeBtn != nullptr)
	{
		if (changeBtn->OnClicked.IsBound() == false)
			changeBtn->OnClicked.AddDynamic(this, &UMainUserWidget::ChangeImageBtnClicked);
	}

	hpProgressBar = Cast<UProgressBar>(GetWidgetFromName(TEXT("HpProgress")));
	manaProgressBar = Cast<UProgressBar>(GetWidgetFromName(TEXT("manaProgress")));
}

void UMainUserWidget::NativePreConstruct()
{
	Super::NativePreConstruct();
	UE_LOG(LogTemp, Log, TEXT("== UMainUserWidget NativePreConstruct =="));
}

void UMainUserWidget::NativeConstruct() 
{
	Super::NativeConstruct();

	UE_LOG(LogTemp, Log, TEXT("== UMainUserWidget NativeConstruct =="));	

	UTextBlock* playerNameText = Cast<UTextBlock>(GetWidgetFromName(TEXT("PlayerNameText")));
	if (playerNameText != nullptr)
	{
		playerNameText->SetText(FText::FromString("WhisperingR"));
	}
}

void UMainUserWidget::NativeDestruct()
{
	Super::NativeDestruct();
	UE_LOG(LogTemp, Log, TEXT("== UMainUserWidget NativeDestruct =="));
}

void UMainUserWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
	//UE_LOG(LogTemp, Log, TEXT("== NativeTick == %f"), InDeltaTime);
	if (hpProgressBar != nullptr) 
	{		
		hpProgressBar->SetPercent(FMath::Max(hpProgressBar->Percent - InDeltaTime * 0.01f, 0.0f));
	}

	if (manaProgressBar != nullptr)
	{
		manaProgressBar->SetPercent(FMath::Max(manaProgressBar->Percent - InDeltaTime * 0.02f, 0.0f));
	}
}

void UMainUserWidget::OptionBtnClicked() 
{
	UE_LOG(LogTemp, Log, TEXT("== OptionBtnClicked =="));	
	
	if (optionPopup != nullptr)
	{
		optionPopup->AddToViewport();
	}
}

void UMainUserWidget::ChangeImageBtnClicked() 
{
	UE_LOG(LogTemp, Log, TEXT("== ChangeImageBtnClicked =="));
	UImage* playerImage = Cast<UImage>(GetWidgetFromName(TEXT("PlayerImage")));
	UTexture2D* texture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), NULL, *FString("/Game/Textures/kda.kda")));	
	playerImage->SetBrushFromTexture(texture);
}
// Fill out your copyright notice in the Description page of Project Settings.

#include "OptionUserWidget.h"

void UOptionUserWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	UE_LOG(LogTemp, Log, TEXT("== UOptionUserWidget NativeOnInitialized =="));

	UButton* closeBtn = Cast<UButton>(GetWidgetFromName(TEXT("CloseBtn")));
	if (closeBtn != nullptr) {
		if (closeBtn->OnClicked.IsBound() == false)
			closeBtn->OnClicked.AddDynamic(this, &UOptionUserWidget::CloseBtnClicked);
	}
}

void UOptionUserWidget::NativePreConstruct()
{
	Super::NativePreConstruct();
	UE_LOG(LogTemp, Log, TEXT("== UOptionUserWidget NativePreConstruct =="));
}

void UOptionUserWidget::NativeConstruct() 
{
	Super::NativeConstruct();
	UE_LOG(LogTemp, Log, TEXT("== UOptionUserWidget NativeConstruct =="));	
}

void UOptionUserWidget::NativeDestruct()
{
	Super::NativeDestruct();
	UE_LOG(LogTemp, Log, TEXT("== UOptionUserWidget NativeDestruct =="));
}

void UOptionUserWidget::CloseBtnClicked() 
{
	UE_LOG(LogTemp, Log, TEXT("== CloseBtnClicked =="));
	this->RemoveFromViewport();
}
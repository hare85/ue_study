// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UMG.h"
#include "OptionUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class STUDYUI_API UOptionUserWidget : public UUserWidget
{
	GENERATED_BODY()
public:	
	virtual void NativeOnInitialized() override;
	virtual void NativePreConstruct() override;	
	virtual void NativeConstruct() override;
	virtual void NativeDestruct() override;

	UFUNCTION()
	void CloseBtnClicked();
};

// Fill out your copyright notice in the Description page of Project Settings.

#include "StudyUIGameModeBase.h"

void AStudyUIGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Log, TEXT("AStudyUIGameModeBase BeginPlay"));
		
	if (mainWidget != nullptr)
	{
		currentWidget = CreateWidget<UUserWidget>(GetWorld(), mainWidget);
		if (currentWidget != nullptr) 
		{
			currentWidget->AddToViewport(); 
		}
	}	
}